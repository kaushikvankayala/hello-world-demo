import requests
import json
from requests.compat import urljoin
import sys

base1 = 'http://172.20.3.185:8081/service/rest/v1/search?repository=JPAPI_' + sys.argv[1] + '&maven.groupId=com.jppl&maven.artifactId='

if sys.argv[1] == 'Production':
	apps = ['proxy-rt4', 'experience-rt4', 'process-rt4', 'system-rt4']
else:
	apps = ['uat-proxy-rt4', 'uat-experience-rt4', 'uat-process-rt4', 'uat-system-rt4']
	
suffix = '&maven.extension=jar&sort=version'

payload = ''

headers = {
  'accept': 'application/json',
  'Authorization': 'Basic YWRtaW46YWRtaW4xMjM='
}

#for x in apps:
#	print(base1 + x + suffix)

for x in apps:
	search_url = base1 + x + suffix
	search_response = requests.request('GET', search_url, headers=headers, data=payload)
	response_data = json.loads(search_response.text)
	items_array = response_data['items']
	items_array_sorted = sorted(items_array, key=lambda k: int(k['version']), reverse=True)
	#print(json.dumps(items_array_sorted, indent = 3))
	base2 = 'http://172.20.3.185:8081/service/rest/v1/components/'
	for i in range(5,len(items_array_sorted)):
		#print(items_array_sorted[i]['version'] + ' - ' + items_array_sorted[i]['id'])
		#print (urljoin(base,items_array_sorted[i]['id']))
		delete_url = urljoin(base2,items_array_sorted[i]['id'])
		delete_response = requests.request('DELETE', delete_url, headers=headers, data=payload)
		print('The version', items_array_sorted[i]['version'], 'of', items_array_sorted[i]['name'], 'is removed and the status code is -', delete_response.status_code)